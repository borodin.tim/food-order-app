import React, { useReducer } from 'react';

const CartContext = React.createContext({
    items: [],
    totalSum: 0,
    addToCart: (item) => { },
    removeFromCart: (id) => { },
    emptyCart: () => { }
});

const defaultCartState = {
    items: [],
    totalSum: 0
};

const cartReducer = (state, action) => {
    let updatedItems = state.items;
    let updatedTotalSum = state.totalSum;

    switch (action.type) {
        case 'ADD_TO_CART':
            const existingItemIndex = state.items.findIndex((item) => item.name === action.val.name);
            if (existingItemIndex >= 0) {
                const updatedItem = {
                    ...state.items[existingItemIndex],
                    amount: parseInt(state.items[existingItemIndex].amount) + parseInt(action.val.amount)
                };
                updatedItems = [...state.items];
                updatedItems[existingItemIndex] = updatedItem;
            } else {
                updatedItems = state.items.concat(action.val);
            }

            updatedTotalSum = state.totalSum + action.val.price * action.val.amount

            return {
                items: updatedItems,
                totalSum: updatedTotalSum
            };
        case 'REMOVE_FROM_CART':
            const existingItemIdx = state.items.findIndex(item => item.id === action.val.id);
            if (existingItemIdx >= 0) {
                if (state.items[existingItemIdx].amount > 1) {
                    const updatedItem = {
                        ...state.items[existingItemIdx],
                        amount: state.items[existingItemIdx].amount - 1
                    }
                    updatedItems = [...state.items];
                    updatedItems[existingItemIdx] = updatedItem;
                } else {
                    updatedItems = state.items.filter(item => item.id !== action.val.id);
                }
                updatedTotalSum = state.totalSum - state.items[existingItemIdx].price;
            }
            if (updatedItems.length === 0) {
                updatedTotalSum = 0.0;
            }

            return {
                items: updatedItems,
                totalSum: updatedTotalSum
            };
        case 'EMPTY_CART':
            return {
                items: [],
                totalSum: 0
            };
        default:
            return defaultCartState;
    }
}

const CartContextProvider = (props) => {
    const [cartState, dispatchCart] = useReducer(cartReducer, defaultCartState);

    const handleAddToCart = (item) => {
        dispatchCart({ type: 'ADD_TO_CART', val: item });
    }

    const handleRemoveFromCart = (id) => {
        dispatchCart({ type: 'REMOVE_FROM_CART', val: id });
    }

    const handleEmptyCart = () => {
        dispatchCart({ type: 'EMPTY_CART' });
    }

    const cartContext = {
        items: cartState.items,
        totalSum: cartState.totalSum,
        addToCart: handleAddToCart,
        removeFromCart: handleRemoveFromCart,
        emptyCart: handleEmptyCart
    };

    return (
        <CartContext.Provider
            value={cartContext}
        >
            {props.children}
        </CartContext.Provider>
    );
}

export {
    CartContext as default,
    CartContextProvider
};