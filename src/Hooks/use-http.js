import { useState, useCallback } from 'react';

const useHttp = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const sendRequest = useCallback(async (transformData, url, options = {}) => {
        try {
            setIsLoading(true);

            const fetchParams = {
                method: options ? options.method : 'GET',
                body: options ? options.body : null,
                headers: options ? options.headers : { 'Content-Type': 'application/json' }
            };

            const response = await fetch(url, fetchParams);

            // TODO: make the request slower and add a loading animation

            // fetch(url, fetchParams)
            //     .then(r => new Promise(
            //         resolve => setTimeout(() => {
            //             resolve(r);
            //         }, 1000)
            //     ))
            //     .then(response => {
            //         if (!response.ok) {
            //             throw new Error('Request failed');
            //         }
            //         response.json().then(
            //             responseData => {
            //                 if (!responseData) {
            //                     throw new Error('No data');
            //                 }
            //                 transformData(responseData);
            //             }
            //         );
            //     });

            if (!response.ok) {
                throw new Error('Request failed');
            }
            const responseData = await response.json();

            if (!responseData) {
                throw new Error('No data');
            }

            transformData(responseData);
        } catch (err) {
            console.log('err.message:', err.message);
            setError(err.message || 'Something went wrong');
        }
        setIsLoading(false);
    }, []);

    return { sendRequest, isLoading, error };
}

export default useHttp;