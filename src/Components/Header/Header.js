import React from 'react';

import styles from './Header.module.css';
import CartButton from '../Cart/CartButton';
import Image from '../UI/Image/Image';

const Header = (props) => {
    return (
        <React.Fragment>
            <header className={styles.content}>
                <h1>React Food Order</h1>
                <CartButton onClick={props.onCartShow} />
            </header>
            <Image className={styles.image}
                src={'https://images.unsplash.com/photo-1547573854-74d2a71d0826?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'}
            />

        </React.Fragment>
    );
}

export default Header;