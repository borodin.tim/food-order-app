import React from 'react';

import styles from './OrderItem.module.css';

const OrderItem = (props) => {
    return (
        <div className={styles.orderItem}>
            <div className={styles.dishName}>{props.item.name}</div>
            <div className={styles.dishPrice}>${parseFloat(props.item.price).toFixed(2)}</div>
            <div className={styles.dishAmount}>x {props.item.amount}</div>
            <div className={styles.totalPrice}>${parseFloat(props.item.price).toFixed(2) * props.item.amount}</div>
        </div>
    );
}

export default OrderItem;