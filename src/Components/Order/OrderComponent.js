import React, { useState } from 'react';

import styles from './OrderComponent.module.css';
import OrderItemsList from './OrderItemsList';
import OrderForm from './OrderForm';
import Modal from '../UI/Modal/Modal';
import Card from '../UI/Card/Card';

const OrderComponent = (props) => {
    return (
        <Modal onClose={props.onClose}>
            <Card className={styles.order}>
                <h3 className={styles.header}>Order Summary</h3>
                <OrderItemsList />
                <OrderForm onClose={props.onClose} />
            </Card>
        </Modal>
    );
}

export default OrderComponent;