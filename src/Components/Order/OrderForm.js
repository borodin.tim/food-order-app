import React, { useState, useContext } from 'react';

import styles from './OrderForm.module.css';
import Button from '../UI/Button/Button';
import useInput from '../../Hooks/use-input';
import useHttp from '../../Hooks/use-http';
import CartContext from '../../Store/CartContext';

const OrderForm = (props) => {
    const ctx = useContext(CartContext);
    const {
        value: name,
        isValid: nameIsValid,
        hasError: nameHasError,
        changeHandler: nameChangeHandler,
        blurHandler: nameBlurHandler,
        resetState: nameResetState
    } = useInput(name => name.length > 0);

    const {
        value: address,
        isValid: addressIsValid,
        hasError: addressHasError,
        changeHandler: addressChangeHandler,
        blurHandler: addressBlurHandler,
        resetState: addressResetState
    } = useInput(address => address.length > 0);

    const formIsValid = nameIsValid && addressIsValid;

    const { sendRequest: storeOrderOnFirebaseRequest, isLoading, error } = useHttp();

    const [isOrderCreated, setIsOrderCreated] = useState(false);

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        if (!formIsValid) {
            return;
        }

        await storeOrderOnFirebaseRequest(() => { }, 'https://food-order-app-2aa9a-default-rtdb.europe-west1.firebasedatabase.app/orders.json', {
            method: 'POST',
            body: JSON.stringify({
                name,
                address,
                dishes: {
                    ...ctx.items
                },
                total: ctx.totalSum
            }),
            headers: { 'Content-Type': 'application/json' }
        });

        nameResetState();
        addressResetState();
        setIsOrderCreated(true);
        emptyCart();
    }

    const emptyCart = () => {
        ctx.emptyCart();
    }

    const nameClassName = nameHasError
        ?
        `${styles['form-control']} ${styles.invalid}`
        :
        `${styles['form-control']}`;
    const addressClassName = addressHasError
        ?
        `${styles['form-control']} ${styles.invalid}`
        :
        `${styles['form-control']}`;

    // TODO: refactor states and JSX expressions
    return (
        <>
            {isLoading && <p className={styles['loading-text ']}>Loading...</p>}
            {error && <p className={styles['error-text']}>{error}</p>}
            {!isLoading && !isOrderCreated &&
                <form onSubmit={handleFormSubmit} className={styles.form}>
                    <h3>Order Details</h3>
                    <div className={nameClassName}>
                        <label htmlFor="name">Name</label>
                        <input
                            autoComplete="off"
                            type="text"
                            id="name"
                            value={name}
                            onChange={nameChangeHandler}
                            onBlur={nameBlurHandler}
                        />
                        {nameHasError && <p className={styles['error-text']}>Name must not be empty</p>}
                    </div>
                    <div className={addressClassName}>
                        <label htmlFor="address">Address</label>
                        <input
                            autoComplete="off"
                            type="text"
                            id="address"
                            value={address}
                            onChange={addressChangeHandler}
                            onBlur={addressBlurHandler}
                        />
                        {addressHasError && <p className={styles['error-text']}>Address must not be empty</p>}
                    </div>
                    <div className={styles.actions}>
                        <Button type="button" className={styles['button--alt']} onClick={props.onClose}>Close</Button>
                        <Button
                            disabled={!formIsValid}
                            className={styles.button}
                            type="submit"
                        >
                            Create Order
                        </Button>
                    </div>
                </form>
            }
            {isOrderCreated &&
                <div className={styles['thank-you']}>
                    <p>Order created</p>
                    <p>Thank you!</p>
                    <Button type="button" className={styles['button--alt']} onClick={props.onClose}>Close</Button>
                </div>

            }
        </>
    );
}

export default OrderForm;