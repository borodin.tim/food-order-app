import React, { useContext } from 'react';

import styles from './OrderItemsList.module.css'
import OrderItem from './OrderItem';
import CartContext from '../../Store/CartContext';

const OrderItemsList = () => {
    const ctx = useContext(CartContext);

    const itemsList = ctx.items.map(item => (
        <OrderItem key={item.id} item={item} />
    ))

    return (
        <>
            {itemsList.length > 0 &&
                <section className={styles.itemsList}>
                    {itemsList}
                    <div className={styles.total}>
                        <span>Total</span>
                        <span>${ctx.totalSum.toFixed(2)}</span>
                    </div>
                </section>
            }
        </>
    );
}

export default OrderItemsList;