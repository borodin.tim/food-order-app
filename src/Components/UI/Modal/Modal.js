import React, { Fragment, useEffect } from 'react';
import ReactDOM from 'react-dom';

import styles from './Modal.module.css';

const Backdrop = (props) => {
    return (<div className={styles.backdrop} onClick={props.onClose} />);
}

const ModalOverlay = (props) => {
    return (
        <div
            className={styles.modal}
            onClick={e => e.stopPropagation()}
        >
            <div>{props.children}</div>
        </div>
    );
}

const Modal = (props) => {
    const closeOnEscapeKeyDown = (e) => {
        if ((e.charCode || e.keyCode) === 27) {
            props.onClose();
        }
    }

    useEffect(() => {
        document.body.addEventListener('keydown', closeOnEscapeKeyDown);
        return function cleanup() {
            document.body.removeEventListener('keydown', closeOnEscapeKeyDown);
        };
    }, []);

    const backdropElement = document.getElementById('backdrop-root');
    const modalOverlayElement = document.getElementById('modal-overlay-root');

    return (
        <Fragment>
            {ReactDOM.createPortal(
                <Backdrop onClose={props.onClose} />,
                backdropElement
            )}
            {ReactDOM.createPortal(
                <ModalOverlay>{props.children}</ModalOverlay>,
                modalOverlayElement
            )}
        </Fragment>
    );
}

export default Modal;