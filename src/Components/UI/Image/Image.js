import styles from './Image.module.css';

const Image = (props) => {
    return (
        <img
            className={`${props.className} ${styles.image}`}
            src={props.src}
            alt="Food"
        />
    );
};

export default Image;