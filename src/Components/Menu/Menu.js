import React, { Fragment } from 'react';

import styles from './Menu.module.css';
import Card from '../UI/Card/Card';
import MenuList from './MenuList';

const Menu = (props) => {
    return (
        <Fragment>
            <Card className={styles.welcome}>
                <h3>Delicious Food, Delivered to You</h3>
                <p>Choose your favorite mean from out broad selection os available meals and enjoy a delicious lunch or dinner at home.</p>
                <p>All our meals are cooked with high-quality ingredients, just-in-time and of course by experienced chefs!</p>
            </Card>
            <MenuList />
        </Fragment>
    );
};

export default Menu;