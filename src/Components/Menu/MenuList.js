import React, { useState, useEffect } from 'react';

import styles from './MenuList.module.css';
import Card from '../UI/Card/Card';
import MenuItem from './MenuItem/MenuItem';
import useHttp from '../../Hooks/use-http';

const MenuList = () => {
    const [menu, setMenu] = useState([]);

    const { sendRequest: fetchMenu, isLoading, error } = useHttp();

    useEffect(() => {
        const transformDataIntoMenuData = (data) => {
            const loadedMenu = [];
            for (const key in data) {
                loadedMenu.push({
                    id: key,
                    name: data[key].name,
                    description: data[key].description,
                    price: data[key].price
                });
            }
            setMenu(loadedMenu);
        };

        setMenu(
            fetchMenu(
                transformDataIntoMenuData,
                'https://food-order-app-2aa9a-default-rtdb.europe-west1.firebasedatabase.app/menu.json'
            )
        );
    }, [fetchMenu]);

    return (
        <Card className={styles.menu} >
            {isLoading && <p className={styles['loading-text']}>Loading...</p>}
            {error && <p className={styles['error-text']}>{error}</p>}
            {!isLoading && !error && menu.map((menuItem) => (
                <MenuItem
                    id={menuItem.id}
                    key={menuItem.id}
                    name={menuItem.name}
                    description={menuItem.description}
                    price={menuItem.price}
                />))
            }
        </Card>
    );
}

export default MenuList;

