import React, { useRef, useContext, useState } from 'react';

import styles from './MenuItemForm.module.css';
import Button from '../../UI/Button/Button';
import Input from '../../UI/Input/Input';
import CartContext from '../../../Store/CartContext';

const MenuItemForm = (props) => {
    const amountInputRef = useRef();
    const ctx = useContext(CartContext);
    const [inputIdValid, setInputIdValid] = useState(true);

    const handleOnSubmit = (event) => {
        event.preventDefault();
        const amountInput = amountInputRef.current.value;

        if (amountInput.trim().length === 0 ||
            amountInput < 1 ||
            amountInput > 5
        ) {
            setInputIdValid(false);
            return;
        }

        ctx.addToCart({
            id: props.id,
            name: props.name,
            price: props.price,
            amount: amountInput
        });
    }

    return (
        <form
            className={styles.purchaseForm}
            onSubmit={handleOnSubmit}
        >
            <Input
                ref={amountInputRef}
                label={'Amount'}
                className={styles.itemsAmount}
                input={{
                    id: 'amount' + props.id,
                    defaultValue: '1',
                    type: 'number',
                    min: '1',
                    max: '5',
                    step: '1',
                }}
            />
            <Button className={styles.button}>+ Add</Button>
            {!inputIdValid && <p>Please enter a valid amount (1-5)</p>}
        </form>
    );
}

export default MenuItemForm;