import React from 'react';

import styles from './MenuItem.module.css'
import MenuItemForm from './MenuItemForm';

const MenuItem = (props) => {
    return (
        <div className={styles.menuItem}>
            <section>
                <div className={styles.dishName}>{props.name}</div>
                <div className={styles.dishDescription}>{props.description}</div>
                <div className={styles.dishPrice}>${parseFloat(props.price).toFixed(2)}</div>
            </section>
            <section>
                <MenuItemForm
                    id={props.id}
                    name={props.name}
                    price={props.price}
                />
            </section>
        </div>
    );
}

export default MenuItem;