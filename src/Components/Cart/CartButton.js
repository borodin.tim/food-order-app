import React, { useContext, useEffect, useState } from 'react';

import styles from './CartButton.module.css';
import CartContext from '../../Store/CartContext';
import CartIcon from './CartIcon';
import Button from '../UI/Button/Button';

const CartButton = (props) => {
    const ctx = useContext(CartContext);
    const [buttonBump, setButtonBump] = useState(false);

    const numberOfCartItems = ctx.items.reduce((curNumber, item) => {
        return curNumber + parseInt(item.amount);
    }, 0);

    const buttonClasses = `${styles.buttonCart} ${buttonBump ? styles.bump : ''}`;

    useEffect(() => {
        if (ctx.items.length === 0) {
            return;
        }
        setButtonBump(true);
        const timer = setTimeout(() => setButtonBump(false), 301);

        return () => {
            clearTimeout(timer);
        }
    }, [ctx.items]);

    return (
        <Button
            className={buttonClasses}
            onClick={props.onClick}
        >
            <span className={styles.icon} >
                <CartIcon />
            </span>
            <span>Your cart</span>
            <span className={styles.itemsAmount}>{numberOfCartItems}</span>
        </Button>
    );
};

export default CartButton;
