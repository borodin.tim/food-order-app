import React from 'react';

import styles from './CartItem.module.css';
import Button from '../UI/Button/Button';

const CartItem = (props) => {


    return (
        <div className={styles.cartItem}>
            <section>
                <div className={styles.dishName}>{props.name}</div>
                <div className={styles.priceSummary}>
                    <div className={styles.dishPrice}>${parseFloat(props.price).toFixed(2)}</div>
                    <div className={styles.dishAmount}>x {props.amount}</div>
                </div>
            </section>
            <section>
                <Button className={styles.buttonAddRemove} onClick={props.onRemoveFromCart}>-</Button>
                <Button className={styles.buttonAddRemove} onClick={props.onAddToCart}>+</Button>
            </section>
        </div>
    );
}

export default CartItem;