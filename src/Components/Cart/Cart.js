import React, { useContext } from 'react';

import styles from './Cart.module.css';
import CartContext from '../../Store/CartContext';
import CartItem from './CartItem';
import Button from '../UI/Button/Button';
import Modal from '../UI/Modal/Modal';
import Card from '../UI/Card/Card';

const Cart = (props) => {
    const ctx = useContext(CartContext);

    const handleAddSingleItemToCart = (item) => {
        ctx.addToCart({
            ...item,
            amount: 1
        });
    }

    const handleRemoveSingleItemFromCart = (id) => {
        ctx.removeFromCart({ id: id });
    }

    const cartItems = ctx.items.map((item) => (
        <CartItem
            key={item.id}
            id={item.id}
            name={item.name}
            price={item.price}
            amount={item.amount}
            onAddToCart={handleAddSingleItemToCart.bind(null, item)}
            onRemoveFromCart={handleRemoveSingleItemFromCart.bind(null, item.id)}
        />
    ));

    const hasItemsInCart = ctx.items.length > 0;

    return (
        <Modal onClose={props.onClose}>
            <Card className={styles.cart}>
                <h3 className={styles.header}>Cart</h3>
                {cartItems.length > 0 && <div>
                    <div className={styles['cart-items']}>
                        {cartItems}
                    </div>
                    <div className={styles.total}>
                        <span>Total</span>
                        <span>${ctx.totalSum.toFixed(2)}</span>
                    </div>
                </div>}
                {cartItems.length === 0 &&
                    <div>Cart is empty</div>
                }
                <div className={styles.actions}>
                    <Button
                        className={styles['button--alt']}
                        onClick={props.onClose}
                    >
                        Close
                    </Button>
                    {hasItemsInCart && <Button
                        className={styles.button}
                        onClick={props.onOrder}
                    >
                        Order
                    </Button>}
                </div>
            </Card>
        </Modal>
    );
}

export default Cart;