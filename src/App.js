import React, { useState } from 'react';

import Header from './Components/Header/Header';
import Menu from './Components/Menu/Menu';
import Cart from './Components/Cart/Cart';
import OrderComponent from './Components/Order/OrderComponent';

function App() {
  const [showCart, setShowCart] = useState(false);
  const [showOrderPage, setShowOrderPage] = useState(false);

  const handleOnCartShow = () => {
    setShowOrderPage(false);
    setShowCart(true);
  }

  const handleOnCartHide = () => {
    setShowCart(false);
  }

  const handleOnOrderPageShow = () => {
    setShowCart(false);
    setShowOrderPage(true);
  }

  const handleOnOrderPageHide = () => {
    setShowOrderPage(false);
  }

  return (
    <React.Fragment>
      <Header onCartShow={handleOnCartShow} />
      <main>
        <Menu />
      </main>
      {showCart && <Cart show={showCart} onClose={handleOnCartHide} onOrder={handleOnOrderPageShow} />}
      {showOrderPage && <OrderComponent show={showOrderPage} onClose={handleOnOrderPageHide} />}
    </React.Fragment>
  );
}

export default App;
