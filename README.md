# React Foor Order

## This project makes use of

#### Hooks
- useContext
- useState
- useEffect
- custom hooks

#### Firebase
- Storing menu 
- Storing order data

#### CSS Modules

---

### TODO:
1. Optimize for mobile devices.
2. Save order data into Firebase.
3. Sort out styling for Order Page.
